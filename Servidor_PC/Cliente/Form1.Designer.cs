﻿
namespace Cliente
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtserver = new System.Windows.Forms.TextBox();
            this.txtmessage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnsend = new System.Windows.Forms.Button();
            this.btnconnect = new System.Windows.Forms.Button();
            this.txtlist = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server:";
            // 
            // txtserver
            // 
            this.txtserver.Location = new System.Drawing.Point(106, 29);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new System.Drawing.Size(393, 23);
            this.txtserver.TabIndex = 1;
            this.txtserver.Text = "25.72.12.219:2620";
            // 
            // txtmessage
            // 
            this.txtmessage.Location = new System.Drawing.Point(106, 243);
            this.txtmessage.Name = "txtmessage";
            this.txtmessage.Size = new System.Drawing.Size(393, 23);
            this.txtmessage.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 246);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Message:";
            // 
            // btnsend
            // 
            this.btnsend.Location = new System.Drawing.Point(308, 272);
            this.btnsend.Name = "btnsend";
            this.btnsend.Size = new System.Drawing.Size(68, 24);
            this.btnsend.TabIndex = 4;
            this.btnsend.Text = "Send";
            this.btnsend.UseVisualStyleBackColor = true;
            this.btnsend.Click += new System.EventHandler(this.btnsend_Click);
            // 
            // btnconnect
            // 
            this.btnconnect.Location = new System.Drawing.Point(398, 272);
            this.btnconnect.Name = "btnconnect";
            this.btnconnect.Size = new System.Drawing.Size(68, 24);
            this.btnconnect.TabIndex = 5;
            this.btnconnect.Text = "Connect";
            this.btnconnect.UseVisualStyleBackColor = true;
            this.btnconnect.Click += new System.EventHandler(this.btnconnect_Click);
            // 
            // txtlist
            // 
            this.txtlist.Location = new System.Drawing.Point(106, 58);
            this.txtlist.Multiline = true;
            this.txtlist.Name = "txtlist";
            this.txtlist.ReadOnly = true;
            this.txtlist.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtlist.Size = new System.Drawing.Size(393, 179);
            this.txtlist.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 336);
            this.Controls.Add(this.txtlist);
            this.Controls.Add(this.btnconnect);
            this.Controls.Add(this.btnsend);
            this.Controls.Add(this.txtmessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtserver);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtserver;
        private System.Windows.Forms.TextBox txtmessage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnsend;
        private System.Windows.Forms.Button btnconnect;
        private System.Windows.Forms.TextBox txtlist;
    }
}

