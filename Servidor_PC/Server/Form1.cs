﻿using SimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SimpleTcpServer server;
        
        
        private void btnconnect_Click(object sender, EventArgs e)
        {
            server.Start();
            txtlist.Text += $"Starting...{Environment.NewLine}";
            btnstart.Enabled = false;
            btnsend.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnsend.Enabled = false;
            server = new SimpleTcpServer(txtserver.Text);
            server.Events.ClientConnected += Events_ClientConnected;
            server.Events.ClientDisconnected += Events_ClientDisconnected;
            server.Events.DataReceived += Events_DataReceived;
        }

        private void Events_DataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtlist.Text += $"{e.IpPort}: {Encoding.UTF8.GetString(e.Data)}{Environment.NewLine}";
            });
        }

        private void Events_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            this.Invoke((MethodInvoker) delegate
            {
                txtlist.Text += $"{e.IpPort} discconnected.{Environment.NewLine}";
                lstLista.Items.Remove(e.IpPort);
            });
        }

        private void Events_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            this.Invoke((MethodInvoker)delegate
            {
                txtlist.Text += $"{e.IpPort} connected.{Environment.NewLine}";
                lstLista.Items.Add(e.IpPort);
            });
        }

        private void btnsend_Click(object sender, EventArgs e)
        {
            if(server.IsListening)  
            {
                if (!string.IsNullOrEmpty(txtmessage.Text) && lstLista.SelectedItem != null)
                {
                    server.Send(lstLista.SelectedItem.ToString(), txtmessage.Text);
                    txtlist.Text += $"Server: {txtmessage.Text}{Environment.NewLine}";
                    txtmessage.Text = string.Empty;
                }
            }
        }
    }
}
