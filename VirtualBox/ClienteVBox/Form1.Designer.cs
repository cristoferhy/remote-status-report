﻿
namespace ClienteVBox
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtlist = new System.Windows.Forms.TextBox();
            this.btnconnect = new System.Windows.Forms.Button();
            this.btnsend = new System.Windows.Forms.Button();
            this.txtmessage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtserver = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txtlist
            // 
            this.txtlist.Location = new System.Drawing.Point(81, 51);
            this.txtlist.Multiline = true;
            this.txtlist.Name = "txtlist";
            this.txtlist.ReadOnly = true;
            this.txtlist.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtlist.Size = new System.Drawing.Size(393, 179);
            this.txtlist.TabIndex = 13;
            // 
            // btnconnect
            // 
            this.btnconnect.Location = new System.Drawing.Point(480, 461);
            this.btnconnect.Name = "btnconnect";
            this.btnconnect.Size = new System.Drawing.Size(68, 24);
            this.btnconnect.TabIndex = 12;
            this.btnconnect.Text = "Connect";
            this.btnconnect.UseVisualStyleBackColor = true;
            this.btnconnect.Click += new System.EventHandler(this.btnconnect_Click);
            // 
            // btnsend
            // 
            this.btnsend.Location = new System.Drawing.Point(406, 461);
            this.btnsend.Name = "btnsend";
            this.btnsend.Size = new System.Drawing.Size(68, 24);
            this.btnsend.TabIndex = 11;
            this.btnsend.Text = "Send";
            this.btnsend.UseVisualStyleBackColor = true;
            this.btnsend.Click += new System.EventHandler(this.btnsend_Click);
            // 
            // txtmessage
            // 
            this.txtmessage.Location = new System.Drawing.Point(81, 236);
            this.txtmessage.Multiline = true;
            this.txtmessage.Name = "txtmessage";
            this.txtmessage.Size = new System.Drawing.Size(393, 219);
            this.txtmessage.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 239);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "Message:";
            // 
            // txtserver
            // 
            this.txtserver.Location = new System.Drawing.Point(81, 22);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new System.Drawing.Size(393, 23);
            this.txtserver.TabIndex = 8;
            this.txtserver.Text = "25.95.217.246:2620";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Server:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(480, 88);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(23, 14);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 497);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.txtlist);
            this.Controls.Add(this.btnconnect);
            this.Controls.Add(this.btnsend);
            this.Controls.Add(this.txtmessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtserver);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Cliente";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtlist;
        private System.Windows.Forms.Button btnconnect;
        private System.Windows.Forms.Button btnsend;
        private System.Windows.Forms.TextBox txtmessage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtserver;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}

